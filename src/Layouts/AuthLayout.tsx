import { Paper, useMediaQuery } from "@mui/material";
import React from "react";
import theme from "../Libs/theme";

export default function AuthLayout({
	children,
}: {
	children: React.ReactNode;
}) {
	return (
		<div>
			<div
				style={{
					backgroundColor: "#F5F7FA",
					height: "100%",
					position: "relative",
					paddingLeft: useMediaQuery(theme.breakpoints.up("md"))
						? theme.spacing(28)
						: 0,
					paddingRight: useMediaQuery(theme.breakpoints.up("md"))
						? theme.spacing(28)
						: 0,
					boxSizing: "border-box",
				}}
			>
				<Paper
					sx={{
						backgroundColor: theme.palette.primary.main,
						minHeight: "100vh",
						height: "100%",
						width: "100%",
						borderRadius: 0,
						paddingX: useMediaQuery(theme.breakpoints.up("md"))
							? theme.spacing(21)
							: theme.spacing(2),
						paddingTop: theme.spacing(2),
						boxSizing: "border-box",
						paddingBottom: theme.spacing(5),
					}}
					elevation={10}
				>
					{children}
				</Paper>
			</div>
		</div>
	);
}
