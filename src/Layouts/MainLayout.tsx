import { alpha, Box, Container, styled, useMediaQuery } from "@mui/material";
import React from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { Header } from "../Components/Header";
import SideBar from "../Components/SideBar";
import drawerWidthAtom from "../Libs/atoms/drawerWidth";
import theme from "../Libs/theme";

export let rightMargin = 50;

export default function MainLayout({
	children,
}: {
	children: React.ReactNode;
}) {
	rightMargin = useMediaQuery(theme.breakpoints.up("sm")) ? 50 : 0;
	const setDrawerWidth = useSetRecoilState(drawerWidthAtom);
	const isMobile = useMediaQuery(theme.breakpoints.down("sm"));

	isMobile ? setDrawerWidth(0) : setDrawerWidth(200);

	return (
		<div
			style={{
				display: "flex",
				height: "100%",
				margin: 0,
				paddingRight: useMediaQuery(theme.breakpoints.up("sm"))
					? 0
					: theme.spacing(1),
				paddingLeft: useMediaQuery(theme.breakpoints.up("sm"))
					? 0
					: theme.spacing(1),
			}}
		>
			{useMediaQuery(theme.breakpoints.up("sm")) && <SideBar />}

			<Main
				sx={{
					border: 1,
					borderColor: alpha(theme.palette.common.black, 0.1),
				}}
			>
				<Header />
				<Container>{children}</Container>
			</Main>
		</div>
	);
}

const Main = styled(Box)(({ theme }) => {
	const drawerWidth = useRecoilValue(drawerWidthAtom);

	return {
		margin: 0,
		width: useMediaQuery(theme.breakpoints.up("sm"))
			? `calc(100% - (${drawerWidth}px + ${rightMargin}px))`
			: "100%",
		height: "100%",
		marginLeft: drawerWidth,
		marginRight: rightMargin,
		boxSizing: "border-box",
		backgroundColor: theme.palette.background.default,
		minHeight: "100vh",
		marginTop: useMediaQuery(theme.breakpoints.down("sm"))
			? theme.spacing(10)
			: 0,
	};
});
