import {
	alpha,
	Avatar,
	Badge,
	Box,
	Button,
	Drawer,
	IconButton,
	Stack,
	styled,
	Toolbar,
	Typography,
} from "@mui/material";
import React from "react";
import theme from "../Libs/theme";
import Logo from "../brand-icon.png";
import {
	Create as CreateIcon,
	FeedOutlined as FeedOutlinedIcon,
	ExploreOutlined as ExploreOutlinedIcon,
	LocalOfferOutlined as LocalOfferOutlinedIcon,
	BookmarkBorderOutlined as BookmarkBorderOutlinedIcon,
	SearchOutlined as SearchOutlinedIcon,
	LightModeOutlined as LightModeOutlinedIcon,
	NotificationsOutlined as NotificationsOutlinedIcon,
} from "@mui/icons-material";
import { useRecoilValue } from "recoil";
import drawerWidthAtom from "../Libs/atoms/drawerWidth";
import { StyledIconButton } from "./Header";

export default function SideBar() {
	const drawerWidth = useRecoilValue(drawerWidthAtom);

	return (
		<>
			<Drawer variant="permanent" sx={{ backgroundColor: "#F5F7FA" }}>
				<Box
					width={drawerWidth}
					height={"100%"}
					sx={{ backgroundColor: "#F5F7FA" }}
				>
					<DrawerHeader>
						<Avatar src={Logo} alt="Logo" sx={{ width: 30, height: 30 }} />
						<Typography
							variant="h6"
							sx={{
								mr: 2,
								color: alpha(theme.palette.text.primary, 0.8),
							}}
						>
							NanoBlog
						</Typography>
					</DrawerHeader>
					<DrawerBody>
						<Button
							variant="contained"
							color="secondary"
							fullWidth
							sx={{
								fontSize: "1.2rem",

								padding: theme.spacing(1.25),
								borderRadius: theme.spacing(1.25),
							}}
							startIcon={<CreateIcon />}
						>
							Write
						</Button>
						<Stack sx={{ mt: theme.spacing(2) }}>
							<ListItem
								color="primary"
								variant="contained"
								startIcon={<FeedOutlinedIcon />}
								disableElevation
							>
								My Feed
							</ListItem>
							<ListItem
								color="primary"
								variant="contained"
								startIcon={<ExploreOutlinedIcon />}
								disableElevation
							>
								Explore
							</ListItem>
							<ListItem
								color="primary"
								variant="contained"
								startIcon={<LocalOfferOutlinedIcon />}
								disableElevation
							>
								Tags
							</ListItem>
							<ListItem
								color="primary"
								variant="contained"
								startIcon={<BookmarkBorderOutlinedIcon />}
								disableElevation
							>
								Bookmarks
							</ListItem>
							<ListItem
								color="primary"
								variant="contained"
								startIcon={<SearchOutlinedIcon />}
								disableElevation
							>
								Search
							</ListItem>
						</Stack>
						<Stack sx={{ mt: theme.spacing(1) }} alignItems={"center"}>
							<StyledIconButton sx={{ width: 50, height: 50 }}>
								<LightModeOutlinedIcon fontSize="large" />
							</StyledIconButton>
							<StyledIconButton
								sx={{ width: 50, height: 50, mt: theme.spacing(1) }}
							>
								<Badge badgeContent={4} color="error">
									<NotificationsOutlinedIcon fontSize="large" />
								</Badge>
							</StyledIconButton>
							<IconButton
								sx={{
									mt: theme.spacing(2),
									width: 50,
									height: 50,
									p: theme.spacing(2),
									border: "2px solid #fff",
									boxShadow: "0px 3px 5px rgba(0,0,0,0.2)",
									"&:hover": {
										backgroundColor: theme.palette.primary.main,
										borderColor: theme.palette.primary.main,
									},
								}}
							>
								<Avatar
									src={Logo}
									alt="Profile"
									sx={{
										width: 45,
										height: 45,
									}}
								/>
							</IconButton>
						</Stack>
					</DrawerBody>
				</Box>
			</Drawer>
		</>
	);
}

const DrawerHeader = styled(Toolbar)(({ theme }) => ({
	display: "flex",
	alignItems: "center",
	justifyContent: "space-evenly",
}));

const DrawerBody = styled(Box)(({ theme }) => ({
	display: "flex",
	flexDirection: "column",
	// alignItems: "center",
	justifyContent: "space-evenly",
	paddingRight: theme.spacing(3),
	paddingTop: theme.spacing(2),
	paddingLeft: theme.spacing(4),
}));

const ListItem = styled(Button)(({ theme }) => ({
	width: "100%",
	height: theme.spacing(6),
	borderRadius: theme.spacing(1.25),
	border: "none",
	backgroundColor: "transparent",
	color: alpha(theme.palette.common.black, 0.7),
	justifyContent: "flex-start",
	paddingLeft: theme.spacing(2),
	textTransform: "none",
	fontSize: "1rem",
	"&:hover": {
		backgroundColor: theme.palette.primary.main,
	},
}));
