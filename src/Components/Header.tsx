import {
	alpha,
	AppBar,
	Avatar,
	Box,
	Button,
	IconButton,
	Stack,
	styled,
	Toolbar,
	Typography,
	useMediaQuery,
	useScrollTrigger,
} from "@mui/material";
import React from "react";
import { useRecoilValue } from "recoil";
import { rightMargin } from "../Layouts/MainLayout";
import drawerWidthAtom from "../Libs/atoms/drawerWidth";
import Logo from "../brand-icon.png";
import theme from "../Libs/theme";
import {
	DarkModeOutlined as DarkModeOutlinedIcon,
	LightModeOutlined as LightModeOutlinedIcon,
} from "@mui/icons-material";
import { Link } from "react-router-dom";
import { sessionAtom } from "../Libs/atoms/sessionAtom";

function ElevationScroll(props: {
	window?: () => Window;
	children: React.ReactElement;
}) {
	const { children, window } = props;
	const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
	// Note that you normally won't need to set the window ref as useScrollTrigger
	// will default to window.
	// This is only being set here because the demo is in an iframe.
	const trigger = useScrollTrigger({
		disableHysteresis: true,
		threshold: 0,
		target: window ? window() : undefined,
	});

	return React.cloneElement(children, {
		elevation: isMobile ? 0 : trigger ? 1 : 0,
	});
}

export const Header = () => {
	const session = useRecoilValue(sessionAtom);
	const isLoggedIn = session.isLoggedIn;

	return isLoggedIn ? <HeaderWithAuthorized /> : <HeaderWithoutAuthorized />;
};

const Offset = styled("div")(({ theme }) => theme.mixins.toolbar, {
	padding: theme.spacing(1),
	backgroundColor: "transparent",
});

const HeaderWithAuthorized = () => {
	const drawerWidth = useRecoilValue(drawerWidthAtom);
	const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
	const appBarRef = React.createRef<HTMLDivElement>();

	return (
		<>
			{isMobile && (
				<ElevationScroll>
					<AppBar
						position="fixed"
						ref={appBarRef}
						color={"transparent"}
						sx={{
							width: `calc(100% - (${drawerWidth}px + ${rightMargin}px))`,
							ml: `${drawerWidth}px`,
							mr: `${rightMargin}px`,
							borderRight: 1,
							borderColor: alpha(theme.palette.common.black, 0.1),
						}}
						elevation={0}
					>
						<Toolbar sx={{ paddingY: theme.spacing(1) }}>
							<Typography
								variant="h6"
								sx={{
									mr: 2,
									color: alpha(theme.palette.common.black, 0.6),
									flexGrow: 1,
								}}
							>
								<Box sx={{ display: "flex" }}>
									<Avatar
										src={Logo}
										alt="Logo"
										sx={{ width: 30, height: 30, mr: 1 }}
									/>
									{"NanoBlog"}
								</Box>
							</Typography>
							<MobileMenu />
						</Toolbar>
					</AppBar>
				</ElevationScroll>
			)}
		</>
	);
};

const HeaderWithoutAuthorized = () => {
	const drawerWidth = useRecoilValue(drawerWidthAtom);
	const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
	const appBarRef = React.createRef<HTMLDivElement>();

	return (
		<>
			<ElevationScroll>
				<AppBar
					position="fixed"
					ref={appBarRef}
					color={
						useMediaQuery(theme.breakpoints.up("sm"))
							? "primary"
							: "transparent"
					}
					sx={{
						width: `calc(100% - (${drawerWidth}px + ${rightMargin}px))`,
						ml: `${drawerWidth}px`,
						mr: `${rightMargin}px`,
						borderRight: 1,
						borderColor: alpha(theme.palette.common.black, 0.1),
					}}
					elevation={0}
				>
					<Toolbar sx={{ paddingY: theme.spacing(1) }}>
						<Typography
							variant="h6"
							sx={{
								mr: 2,
								color: alpha(theme.palette.common.black, 0.6),
								flexGrow: 1,
							}}
						>
							{!isMobile ? (
								" Blogging as a developer, done right!"
							) : (
								<Box sx={{ display: "flex" }}>
									<Avatar
										src={Logo}
										alt="Logo"
										sx={{ width: 30, height: 30, mr: 1 }}
									/>
									{"NanoBlog"}
								</Box>
							)}
						</Typography>
						{isMobile ? <MobileMenu /> : <DesktopMenu />}
					</Toolbar>
				</AppBar>
			</ElevationScroll>
			{!isMobile && <Offset />}
		</>
	);
};

const StyledButton = styled(Button)(({ theme }) => ({
	paddingTop: theme.spacing(1.25),
	paddingBottom: theme.spacing(1.25),
	textTransform: "none",
	fontWeight: "bold",
	fontSize: "1.1rem",
	borderRadius: theme.spacing(1.5),
	borderColor: alpha(theme.palette.common.black, 0.2),
	"&:hover": {
		boxShadow: "0px 2px 5px rgba(0,0,0,0.2)",
	},
}));

const DesktopMenu = () => {
	return (
		<Stack direction={"row"} justifyContent="space-between" spacing={2}>
			<Link
				to="auth/login"
				style={{ textDecoration: "none", color: theme.palette.common.black }}
			>
				<StyledButton
					variant="outlined"
					color="inherit"
					sx={{
						fontSize: "1.05rem",
					}}
					disableElevation
				>
					Login
				</StyledButton>
			</Link>

			<Link
				to="auth/register"
				style={{ textDecoration: "none", color: theme.palette.common.white }}
			>
				<StyledButton variant="contained" color="secondary" disableElevation>
					Create an account
				</StyledButton>
			</Link>
		</Stack>
	);
};

const MobileMenu = () => {
	return (
		<Stack direction={"row"}>
			<StyledIconButton sx={{ width: 45, height: 45 }}>
				<DarkModeOutlinedIcon />
			</StyledIconButton>
			<StyledIconButton sx={{ width: 45, height: 45 }}>
				<LightModeOutlinedIcon />
			</StyledIconButton>
			<IconButton
				sx={{
					width: 45,
					height: 45,
					backgroundColor: "transparent",
					border: "1px solid #fff",
					padding: 2,
					ml: 2,
				}}
			>
				<Avatar
					src={Logo}
					alt="Logo"
					sx={{
						width: 45,
						height: 45,
						backgroundColor: "transparent",
						border: "3px solid #fff",
						padding: "2px",
					}}
				/>
			</IconButton>
		</Stack>
	);
};

export const StyledIconButton = styled(IconButton)(({ theme }) => {
	return {
		width: 45,
		height: 45,
		borderRadius: theme.spacing(1.5),
		borderColor: alpha(theme.palette.common.black, 0.2),
		"&:hover": {
			backgroundColor: theme.palette.primary.main,
		},
	};
});
