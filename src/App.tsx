import React from "react";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";

// Pages
import Home from "./Pages/Home";
import Auth from "./Pages/Auth/Auth";
import Login from "./Pages/Auth/Login";
import Register from "./Pages/Auth/Register";
import { Box } from "@mui/material";

function App() {
	return (
		<BrowserRouter>
			<Box sx={{ backgroundColor: "#F5F7FA" }}>
				<Routes>
					<Route path="/" element={<Home />} />
					<Route path="auth">
						<Route index element={<Auth />} />
						<Route path="login" element={<Login />} />
						<Route path="register" element={<Register />} />
					</Route>
				</Routes>
			</Box>
		</BrowserRouter>
	);
}

export default App;
