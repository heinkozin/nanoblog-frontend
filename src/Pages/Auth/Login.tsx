import {
	Button,
	FormLabel,
	LinearProgress,
	Stack,
	Typography,
	useMediaQuery,
} from "@mui/material";
import axios from "axios";
import { Formik, Form, Field } from "formik";
import { TextField } from "formik-mui";
import * as React from "react";
import { useRecoilState } from "recoil";
import AuthLayout from "../../Layouts/AuthLayout";
import { sessionAtom } from "../../Libs/atoms/sessionAtom";
import theme from "../../Libs/theme";
import { Navigate } from "react-router-dom";

interface Values {
	email: string;
	password: string;
}

function Login() {
	const [token, setToken] = useRecoilState(sessionAtom);

	if (!token.isLoggedIn) {
		return (
			<AuthLayout>
				<Typography
					variant="h4"
					component="p"
					sx={{
						mb: 5,
						fontSize: { sm: "2rem", md: "2rem", lg: "2rem" },
					}}
				>
					Login into your account
				</Typography>
				<Formik
					initialValues={{
						email: "",
						password: "",
					}}
					validate={(values) => {
						const errors: Partial<Values> = {};
						if (!values.email) {
							errors.email = "Required";
						} else if (
							!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
						) {
							errors.email = "Invalid email address";
						}
						return errors;
					}}
					onSubmit={(values, { setSubmitting }) => {
						setTimeout(async () => {
							setSubmitting(false);

							// send values to api with axios
							const data = new FormData();
							data.append("username", values.email);
							data.append("password", values.password);
							await axios
								.post("http://localhost/api/v1/login/access-token", data)
								.then((res) => {
									setToken({
										token: res.data.access_token,
										token_type: res.data.token_type,
										isLoggedIn: true,
									});
									window.localStorage.setItem("token", res.data.access_token);
								});
						}, 500);
					}}
				>
					{({ submitForm, isSubmitting }) => (
						<Form
							style={{
								width: "100%",
							}}
						>
							<Stack
								direction={"row"}
								alignItems={"center"}
								sx={{ width: "100%", mb: 3, mt: theme.spacing(10) }}
							>
								<FormLabel
									sx={{
										width: "30%",
										fontWeight: "bold",
										color: theme.palette.common.black,
									}}
								>
									Email:
								</FormLabel>
								<Field
									component={TextField}
									name="email"
									type="email"
									placeholder="Email"
									sx={{
										flexGrow: 1,
										"&:focus": {
											borderColor: theme.palette.common.black,
										},
									}}
								/>
							</Stack>
							<Stack
								direction={"row"}
								alignItems={"center"}
								sx={{ width: "100%", mb: 5 }}
							>
								<FormLabel
									sx={{
										width: "30%",
										fontWeight: "bold",
										color: theme.palette.common.black,
									}}
								>
									Password:
								</FormLabel>
								<Field
									component={TextField}
									name="password"
									type="password"
									placeholder="Password"
									sx={{ flexGrow: 1 }}
								/>
							</Stack>
							{isSubmitting && <LinearProgress />}
							<br />
							<Button
								variant="contained"
								color="secondary"
								disabled={isSubmitting}
								onClick={submitForm}
								sx={{
									padding: theme.spacing(1.25, 2.5),
									textTransform: "none",
									fontWeight: "bold",
									borderRadius: theme.spacing(1.5),
									mt: theme.spacing(5),
									// eslint-disable-next-line react-hooks/rules-of-hooks
									fontSize: useMediaQuery(theme.breakpoints.up("md"))
										? "1.1rem"
										: "1rem",
								}}
							>
								Login
							</Button>
						</Form>
					)}
				</Formik>
			</AuthLayout>
		);
	} else return <Navigate to="/" />;
}

export default Login;
