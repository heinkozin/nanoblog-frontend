import {
	alpha,
	Button,
	FormControl,
	InputBase,
	Paper,
	Typography,
	useMediaQuery,
} from "@mui/material";
import React from "react";
import theme from "../../Libs/theme";

export default function Auth() {
	return (
		<div
			style={{
				backgroundColor: "#F5F7FA",
				width: "100vw",
				height: "100vh",
				paddingLeft: useMediaQuery(theme.breakpoints.up("md"))
					? theme.spacing(28)
					: 0,
				paddingRight: useMediaQuery(theme.breakpoints.up("md"))
					? theme.spacing(28)
					: 0,
				boxSizing: "border-box",
			}}
		>
			<Paper
				sx={{
					backgroundColor: theme.palette.primary.main,
					height: "100%",
					width: "100%",
					borderRadius: 0,
					paddingX: useMediaQuery(theme.breakpoints.up("md"))
						? theme.spacing(21)
						: theme.spacing(2),
					paddingTop: theme.spacing(2),
					boxSizing: "border-box",
				}}
				elevation={10}
			>
				<Typography variant="h4">NanoBlog</Typography>
				<Typography
					variant="h6"
					sx={{ mt: theme.spacing(2), fontWeight: "bold" }}
				>
					Sign in / Create an account
				</Typography>
				<Typography variant="body1" sx={{ mt: theme.spacing(5) }}>
					Sign in using a secure link
				</Typography>
				<FormControl fullWidth>
					<InputBase
						placeholder="Enter your email"
						fullWidth
						sx={{
							border: "1.2px solid",
							borderColor: alpha(theme.palette.common.black, 0.2),
							padding: theme.spacing(1.3),
							borderRadius: theme.spacing(1),
							mt: theme.spacing(1),
						}}
					/>
				</FormControl>
				<Button
					color="secondary"
					variant="contained"
					sx={{
						mt: theme.spacing(2),
						padding: theme.spacing(1.5, 2.5),
						textTransform: "none",
						fontWeight: "bold",
						fontSize: useMediaQuery(theme.breakpoints.up("md"))
							? "1.1rem"
							: "1rem",
						borderRadius: theme.spacing(1.5),
					}}
				>
					Submit
				</Button>
			</Paper>
		</div>
	);
}
