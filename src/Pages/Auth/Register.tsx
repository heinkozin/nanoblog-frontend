import {
	alpha,
	Badge,
	Box,
	Button,
	Divider,
	FormControl,
	InputBase,
	Link,
	Paper,
	Stack,
	Typography,
	useMediaQuery,
} from "@mui/material";
import { red } from "@mui/material/colors";
import {
	Search as SearchIcon,
	Interests as InterestsIcon,
} from "@mui/icons-material";
import React from "react";
import theme from "../../Libs/theme";
import AuthLayout from "../../Layouts/AuthLayout";

export default function Register() {
	const [step, setIsForm] = React.useState<number>(1);

	return (
		<AuthLayout>
			{step === 1 && <StepOne setIsForm={setIsForm} />}
			{step === 2 && <StepTwo />}

			<Stack direction="row" justifyContent={"end"}>
				<Button
					color="secondary"
					variant="contained"
					disabled={step === 2}
					sx={{
						padding: theme.spacing(1.25, 2.5),
						textTransform: "none",
						fontWeight: "bold",
						fontSize: useMediaQuery(theme.breakpoints.up("md"))
							? "1.1rem"
							: "1rem",
						borderRadius: theme.spacing(1.5),
						mt: theme.spacing(5),
					}}
					onClick={() => {
						setIsForm(step + 1);
					}}
				>
					Next
				</Button>
			</Stack>
		</AuthLayout>
	);
}

const StepOne = ({
	setIsForm,
}: {
	setIsForm: React.Dispatch<React.SetStateAction<number>>;
}) => {
	return (
		<div style={{ maxWidth: "100%" }}>
			<Typography variant="h4">Create Account</Typography>
			<Stack
				sx={{ mt: theme.spacing(5), fontSize: "1.1rem" }}
				spacing={3}
				// direction={"row"}
			>
				{/* NOTE: Test */}

				<FormControl
					fullWidth
					sx={{
						display: "flex",
						flexDirection: "row",
						alignItems: "center",
						justifyContent: "space-between",
					}}
				>
					<Typography variant="h6">Fullname</Typography>

					<InputBase
						placeholder="Enter your name"
						sx={{
							border: "1.2px solid",
							borderColor: alpha(theme.palette.common.black, 0.2),
							padding: theme.spacing(1.3),
							borderRadius: theme.spacing(1),
							width: useMediaQuery(theme.breakpoints.up("sm")) ? "10cm" : "6cm",
						}}
					/>
				</FormControl>
				<FormControl
					fullWidth
					sx={{
						display: "flex",
						flexDirection: "row",
						alignItems: "center",
						justifyContent: "space-between",
					}}
				>
					<Typography variant="h6" sx={{ float: "left" }}>
						Email
					</Typography>

					<InputBase
						placeholder="Enter your name"
						sx={{
							border: "1.2px solid",
							borderColor: alpha(theme.palette.common.black, 0.2),
							padding: theme.spacing(1.3),
							borderRadius: theme.spacing(1),
							width: useMediaQuery(theme.breakpoints.up("sm")) ? "10cm" : "6cm",
						}}
					/>
				</FormControl>
				<FormControl
					fullWidth
					sx={{
						display: "flex",
						flexDirection: "row",
						justifyContent: "space-between",
						alignItems: "center",
					}}
				>
					<Typography variant="h6" sx={{ float: "left" }}>
						Username
					</Typography>

					<InputBase
						placeholder="Enter your username"
						sx={{
							border: "1.2px solid",
							borderColor: alpha(theme.palette.common.black, 0.2),
							padding: theme.spacing(1.3),
							borderRadius: theme.spacing(1),
							width: useMediaQuery(theme.breakpoints.up("sm")) ? "10cm" : "6cm",
						}}
					/>
				</FormControl>

				{/* <Stack
			direction={"row"}
			justifyContent={"space-between"}
			alignItems={"center"}
		>
			<Typography variant="h6">Fullname: </Typography>
			<FormControl>
				<InputBase
					placeholder="Enter your name"
					sx={{
						border: "1.2px solid",
						borderColor: alpha(theme.palette.common.black, 0.2),
						padding: theme.spacing(1.3),
						borderRadius: theme.spacing(1),
						mt: theme.spacing(1),
						width: "10cm",
					}}
				/>
			</FormControl>
		</Stack>
		<Stack
			direction={"row"}
			justifyContent={"space-between"}
			alignItems={"center"}
		>
			<Typography variant="h6">Email:</Typography>
			<FormControl>
				<InputBase
					placeholder="Enter your email"
					sx={{
						border: "1.2px solid",
						borderColor: alpha(theme.palette.common.black, 0.2),
						padding: theme.spacing(1.3),
						borderRadius: theme.spacing(1),
						mt: theme.spacing(1),
						width: "10cm",
					}}
				/>
			</FormControl>
		</Stack>
		<Stack
			direction={"row"}
			justifyContent={"space-between"}
			alignItems={"center"}
		>
			<Typography variant="h6">Password:</Typography>
			<FormControl>
				<InputBase
					placeholder="Enter password"
					sx={{
						border: "1.2px solid",
						borderColor: alpha(theme.palette.common.black, 0.2),
						padding: theme.spacing(1.3),
						borderRadius: theme.spacing(1),
						mt: theme.spacing(1),
						width: "10cm",
					}}
				/>
			</FormControl>
		</Stack>
		<Stack
			direction={"row"}
			justifyContent={"space-between"}
			alignItems={"center"}
		>
			<Typography variant="h6">Confirm Password:</Typography>
			<FormControl>
				<InputBase
					placeholder="Enter confirm password"
					sx={{
						border: "1.2px solid",
						borderColor: alpha(theme.palette.common.black, 0.2),
						padding: theme.spacing(1.3),
						borderRadius: theme.spacing(1),
						mt: theme.spacing(1),
						width: "10cm",
					}}
				/>
			</FormControl>
		</Stack> */}
			</Stack>
			<Stack
				direction={"row"}
				sx={{
					mt: theme.spacing(5),
					alignItems: "center",
					justifyContent: "space-between",
				}}
			>
				<Link
					href="/termsandconditions"
					variant="body1"
					sx={{ color: red[500], textDecoration: "underline" }}
				>
					Terms and conditions
				</Link>
			</Stack>
		</div>
	);
};

interface TagProps {
	id: number;
	tag: string;
}

const StepTwo = () => {
	const [tags, setTags] = React.useState<TagProps[]>([
		{ id: 1, tag: "#Web" },
		{ id: 2, tag: "#Python" },
		{ id: 3, tag: "#PHP" },
		{ id: 4, tag: "#JavaScript" },
		{ id: 5, tag: "#React" },
		{ id: 6, tag: "#Vue" },
		{ id: 7, tag: "#Flutter" },
		{ id: 8, tag: "#ReactNative" },
		{ id: 9, tag: "#Database" },
		{ id: 10, tag: "#Java" },
	]);

	const [followedTags, setFollowedTags] = React.useState<TagProps[]>([]);

	const handleTag = (tag: TagProps) => {
		const newFollowedTags = [...followedTags];
		const allTags = [...tags];
		const index = newFollowedTags.findIndex((t) => t.id === tag.id);
		if (index === -1) {
			newFollowedTags.push(tag);
			allTags.splice(
				allTags.findIndex((t) => t.id === tag.id),
				1
			);
		} else {
			newFollowedTags.splice(index, 1);
			allTags.push(tag);
		}
		setFollowedTags(newFollowedTags);
		setTags(allTags.sort((a, b) => a.tag.localeCompare(b.tag)));
	};

	return (
		<div style={{ width: "100%" }}>
			{/* NOTE: Bar */}
			<Stack
				direction={"row"}
				justifyContent={"space-between"}
				alignItems={"center"}
				sx={{
					mt: theme.spacing(5),
					mb: theme.spacing(5),
				}}
			>
				<Typography variant="h6" sx={{ fontWeight: "bold" }}>
					Follow your interest{" "}
					<Badge color="secondary" badgeContent={followedTags.length}>
						<InterestsIcon />
					</Badge>
				</Typography>
				<FormControl
					sx={{
						display: "flex",
						flexDirection: "row",
						justifyContent: "space-between",
						alignItems: "center",
					}}
				>
					<InputBase
						placeholder="Search for tags"
						sx={{
							border: "1.2px solid",
							borderColor: alpha(theme.palette.common.black, 0.2),
							padding: theme.spacing(1.3),
							borderRadius: theme.spacing(1),
						}}
						startAdornment={
							<SearchIcon sx={{ color: theme.palette.common.black }} />
						}
					/>
				</FormControl>
			</Stack>
			{/* NOTE: Followed tags */}
			<Typography variant="h6" sx={{ fontWeight: "bold" }}>
				Followed tags
			</Typography>
			<Divider />
			<Box minHeight={"100px"}>
				<Stack direction={"row"} sx={{ flexWrap: "wrap" }}>
					{followedTags.map((item) => (
						<Tag key={item.id} item={item} onClick={handleTag} followed />
					))}
				</Stack>
			</Box>

			{/* NOTE: Tags */}
			<Typography variant="h6" sx={{ fontWeight: "bold" }}>
				Tags
			</Typography>
			<Divider />
			<Box minHeight={"100px"}>
				<Stack direction={"row"} sx={{ flexWrap: "wrap", width: "100%" }}>
					{tags.map((item) => (
						// Remove followed tags from list
						// tags.find((tag) => tag.id === item.id) ? null : (
						// 	<Tag
						// 		key={item.id}
						// 		item={item}
						// 		setFollowedTags={setFollowedTags}
						// 	/>
						// )
						<Tag key={item.id} item={item} onClick={handleTag} />
					))}
				</Stack>
			</Box>
		</div>
	);
};

const Tag = ({
	item,
	onClick,
	followed = false,
}: {
	item: { id: number; tag: string };
	onClick: (tag: TagProps) => void;
	followed?: boolean;
}) => {
	const [isFollow, setFollow] = React.useState<boolean>(followed);

	const toggleFollow = () => {
		if (isFollow) {
			// NOTE: Unfollow
			setFollow(false);
			onClick(item);
		} else {
			// NOTE: Follow
			setFollow(true);
			onClick(item);
		}
	};

	return (
		<Paper
			sx={{
				padding: theme.spacing(1.5, 1),
				margin: theme.spacing(1),
				float: "left",
			}}
			elevation={5}
		>
			<Stack direction={"row"} alignItems={"center"}>
				<Typography>{item.tag}</Typography>
				<Button
					color="secondary"
					variant="contained"
					sx={{
						ml: theme.spacing(1),
						fontSize: "0.8rem",
						textTransform: "none",
					}}
					// disabled={followed}
					size="small"
					onClick={() => toggleFollow()}
				>
					{isFollow ? "Unfollow" : "Follow"}
				</Button>
			</Stack>
		</Paper>
	);
};
