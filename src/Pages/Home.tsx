import { alpha, Box, Button, Typography, useMediaQuery } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";
import { useRecoilValue } from "recoil";
import MainLayout from "../Layouts/MainLayout";
import { sessionAtom } from "../Libs/atoms/sessionAtom";
import theme from "../Libs/theme";

export default function Home() {
	const session = useRecoilValue(sessionAtom);
	return (
		<div style={{ height: "100%", margin: 0 }}>
			<MainLayout>
				{!session.isLoggedIn ? (
					<HomeWithoutAuthorized />
				) : (
					<HomeWithAuthorized />
				)}
			</MainLayout>
		</div>
	);
}

const HomeWithAuthorized = () => {
	return <Box>Home</Box>;
};

const HomeWithoutAuthorized = () => {
	return (
		<Box
			sx={{
				minHeight: "500px",
				width: "100%",
				display: "flex",
				flexDirection: "column",
				justifyContent: "center",
				alignItems: "center",
				textAlign: "center",
			}}
		>
			<Typography variant="h3" sx={{ fontWeight: "bold" }}>
				Everything you need to start blogging as a developer!
			</Typography>
			<Typography
				variant="h6"
				sx={{
					color: alpha(theme.palette.common.black, 0.55),
					mt: theme.spacing(2),
				}}
			>
				Own your content, share ideas, and
				<br /> connect with the global dev community!
			</Typography>
			<Link
				to="auth/register"
				style={{
					textDecoration: "none",
					color: theme.palette.common.white,
				}}
			>
				<Button
					variant="contained"
					color="secondary"
					sx={{
						mt: theme.spacing(4),
						px: theme.spacing(5),
						py: theme.spacing(1.75),
						textTransform: "none",
						fontSize: "1.1rem",
						fontWeight: "bold",
						borderRadius: theme.spacing(1),
					}}
				>
					{useMediaQuery(theme.breakpoints.up("sm"))
						? "Start your personal blog for FREE!"
						: "Get Started"}
				</Button>
			</Link>
		</Box>
	);
};
