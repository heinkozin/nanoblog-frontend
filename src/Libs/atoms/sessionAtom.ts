import { atom, selector } from "recoil";

const token = window.localStorage.getItem("token");
const isLoggedIn = token ? true : false;

const sessionAtom = atom({
	key: "sessionAtom",
	default: {
		token: token,
		token_type: "bearer",
		isLoggedIn: isLoggedIn,
	},
});

const user = selector({
	key: "userAtom",
	get: ({ get }) => {
		const session = get(sessionAtom);
		if (session.token) {
			return session.token;
		}
		return null;
	},
});

export { sessionAtom, user };
