import { atom } from "recoil";

const drawerWidthAtom = atom({
	key: "drawerWidthAtom",
	default: 200,
});

export default drawerWidthAtom;
