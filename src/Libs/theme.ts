import { createTheme, responsiveFontSizes } from "@mui/material";

let theme = createTheme({
	palette: {
		mode: "light",
		primary: {
			main: "#FFFFFF",
		},
		secondary: {
			main: "#2962FF",
		},
		warning: {
			main: "#FF9100",
		},
		background: {
			default: "#ffffff",
			paper: "#F5F7FA",
		},
	},
});

theme = responsiveFontSizes(theme);

export default theme;
